/**
 * Includes
 */
var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var wrap = require('gulp-wrap');
var tap = require('gulp-tap');
var util = require('gulp-util');
var debug = require('gulp-debug');
var path = require('path');

var pathToAssets = '**/Resources/private/assets/view/';
var pathToEditAssets = '**/Resources/private/assets/edit/';

var pathToKitt3n = 'vendor/kitt3n/';
var pathToCss = 'css/';
var pathToScss = 'scss/';

var pathToCompiledFiles = 'web/assets/view/';
var pathToCompiledEditFiles = 'web/assets/edit/';

var paths = {
    src: {
        css: [
            pathToKitt3n + 'pimcore-layouts/' + pathToAssets + pathToScss + '*.scss'
        ],
        editCss: [
            pathToKitt3n + 'pimcore-layouts/' + pathToEditAssets + pathToScss + '*.scss'
        ]
    },
    dest: {
        css: pathToCompiledFiles + pathToCss,
        editCss: pathToCompiledEditFiles + pathToCss
    }
};

var watcherPaths = {
    src: {
        css: [
            pathToKitt3n + 'pimcore-layouts/' + pathToAssets + pathToScss + '*.scss'
        ],
        editCss: [
            pathToKitt3n + 'pimcore-layouts/' + pathToEditAssets + pathToScss + '*.scss'
        ]
    }
};

/**
 * Scss => Css
 */
gulp.task('css', function() {
    gulp.src(
        paths.src.css
    )
        .pipe(concat('index.css'))
        .pipe(sass({
            paths: [ 'web' ]
        }).on('error', sass.logError))
        .pipe(gulp.dest(paths.dest.css))
        .pipe(rename('index.min.css'))
        .pipe(autoprefixer({
            browsers: ['last 3 versions', 'safari 7', 'safari 8', 'edge 15', 'ie 9-11'],
            grid: true
        }))
        .pipe(cleanCSS(
            {
                level: {
                    2: {
                        // mergeAdjacentRules: false, // controls adjacent rules merging; defaults to true
                        // mergeIntoShorthands: false, // controls merging properties into shorthands; defaults to true
                        mergeMedia: false, // controls `@media` merging; defaults to true
                        mergeNonAdjacentRules: false // controls non-adjacent rule merging; defaults to true
                        // mergeSemantically: true // controls semantic merging; defaults to false
                        // overrideProperties: true, // controls property overriding based on understandability; defaults to true
                        // reduceNonAdjacentRules: true, // controls non-adjacent rule reducing; defaults to true
                        // removeDuplicateFontRules: true, // controls duplicate `@font-face` removing; defaults to true
                        // removeDuplicateMediaBlocks: true, // controls duplicate `@media` removing; defaults to true
                        // removeDuplicateRules: true, // controls duplicate rules removing; defaults to true
                        // restructureRules: true // controls rule restructuring; defaults to false
                    }

                }
            }
        ))
        .pipe(gulp.dest(paths.dest.css))
});

/**
 * Scss => Css
 */
gulp.task('editCss', function() {
    gulp.src(
        paths.src.editCss
    )
        .pipe(concat('index.css'))
        .pipe(sass({
            paths: [ 'web' ]
        }).on('error', sass.logError))
        .pipe(gulp.dest(paths.dest.editCss))
        .pipe(rename('index.min.css'))
        .pipe(autoprefixer({
            browsers: ['last 3 versions', 'safari 7', 'safari 8', 'edge 15', 'ie 9-11'],
            grid: true
        }))
        .pipe(cleanCSS(
            {
                level: {
                    2: {
                        // mergeAdjacentRules: false, // controls adjacent rules merging; defaults to true
                        // mergeIntoShorthands: false, // controls merging properties into shorthands; defaults to true
                        mergeMedia: false, // controls `@media` merging; defaults to true
                        mergeNonAdjacentRules: false // controls non-adjacent rule merging; defaults to true
                        // mergeSemantically: true // controls semantic merging; defaults to false
                        // overrideProperties: true, // controls property overriding based on understandability; defaults to true
                        // reduceNonAdjacentRules: true, // controls non-adjacent rule reducing; defaults to true
                        // removeDuplicateFontRules: true, // controls duplicate `@font-face` removing; defaults to true
                        // removeDuplicateMediaBlocks: true, // controls duplicate `@media` removing; defaults to true
                        // removeDuplicateRules: true, // controls duplicate rules removing; defaults to true
                        // restructureRules: true // controls rule restructuring; defaults to false
                    }

                }
            }
        ))
        .pipe(gulp.dest(paths.dest.editCss))
});

/**
 * Fly Willy fly ;)
 */
gulp.task(
    'default',
    [
        'css',
        'editCss'
    ],
    function() {
        gulp.watch(watcherPaths.src.css, ['css'])
            .on('change', function(evt) {
                console.log(
                    '[watcher] File ' + evt.path + ' was ' + evt.type + ', compiling...'
                );
            });
        gulp.watch(watcherPaths.src.css, ['editCss'])
            .on('change', function(evt) {
                console.log(
                    '[watcher] File ' + evt.path + ' was ' + evt.type + ', compiling...'
                );
            });
    }
);