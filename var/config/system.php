<?php 

return [
    "general" => [
        "timezone" => "Europe/Berlin",
        "language" => "en",
        "validLanguages" => "en,de",
        "path_variable" => "",
        "domain" => "",
        "redirect_to_maindomain" => FALSE,
        "fallbackLanguages" => [
            "en" => "",
            "de" => "en"
        ],
        "defaultLanguage" => "de",
        "loginscreencustomimage" => "",
        "disableusagestatistics" => FALSE,
        "debug_admin_translations" => FALSE,
        "devmode" => FALSE,
        "instanceIdentifier" => "",
        "show_cookie_notice" => FALSE
    ],
    "database" => [
        "params" => [
            "username" => "dev",
            "password" => "dev",
            "dbname" => "dev_pim",
            "host" => "mysqlpim",
            "port" => 3306
        ]
    ],
    "documents" => [
        "versions" => [
            "steps" => 10,
            "days" => NULL
        ],
        "error_pages" => [
            "default" => "/"
        ],
        "createredirectwhenmoved" => FALSE,
        "allowtrailingslash" => "no",
        "generatepreview" => TRUE
    ],
    "objects" => [
        "versions" => [
            "steps" => 10,
            "days" => NULL
        ]
    ],
    "assets" => [
        "versions" => [
            "steps" => 10,
            "days" => NULL
        ],
        "icc_rgb_profile" => "",
        "icc_cmyk_profile" => "",
        "hide_edit_image" => FALSE,
        "disable_tree_preview" => FALSE
    ],
    "services" => [
        "google" => [
            "client_id" => "",
            "email" => "",
            "simpleapikey" => "",
            "browserapikey" => ""
        ]
    ],
    "cache" => [
        "excludeCookie" => "",
        "enabled" => FALSE,
        "lifetime" => NULL,
        "excludePatterns" => ""
    ],
    "httpclient" => [
        "adapter" => "Socket",
        "proxy_host" => "",
        "proxy_port" => "",
        "proxy_user" => "",
        "proxy_pass" => ""
    ],
    "email" => [
        "sender" => [
            "name" => "",
            "email" => ""
        ],
        "return" => [
            "name" => "",
            "email" => ""
        ],
        "method" => "sendmail",
        "smtp" => [
            "host" => "",
            "port" => "",
            "ssl" => NULL,
            "name" => "",
            "auth" => [
                "method" => NULL,
                "username" => "",
                "password" => ""
            ]
        ],
        "debug" => [
            "emailaddresses" => ""
        ]
    ],
    "newsletter" => [
        "sender" => [
            "name" => "",
            "email" => ""
        ],
        "return" => [
            "name" => "",
            "email" => ""
        ],
        "method" => NULL,
        "smtp" => [
            "host" => "",
            "port" => "",
            "ssl" => NULL,
            "name" => "",
            "auth" => [
                "method" => NULL,
                "username" => "",
                "password" => ""
            ]
        ],
        "usespecific" => FALSE,
        "debug" => NULL
    ],
    "branding" => [
        "color_login_screen" => "",
        "color_admin_interface" => ""
    ],
    "webservice" => [
        "enabled" => FALSE
    ],
    "applicationlog" => [
        "mail_notification" => [
            "send_log_summary" => FALSE,
            "filter_priority" => NULL,
            "mail_receiver" => ""
        ],
        "archive_treshold" => "30",
        "archive_alternative_database" => ""
    ]
];
