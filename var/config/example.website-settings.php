<?php 

return [
    4 => [
        "id" => 4,
        "name" => "bam.root.document",
        "language" => "de",
        "type" => "document",
        "data" => 2,
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1564062231
    ],
    5 => [
        "id" => 5,
        "name" => "bam.root.tag",
        "language" => "",
        "type" => "text",
        "data" => "1",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1562768854
    ],
    6 => [
        "id" => 6,
        "name" => "bam.custom.logo",
        "language" => "",
        "type" => "asset",
        "data" => 4,
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1564062457
    ],
    7 => [
        "id" => 7,
        "name" => "bam.custom.visual",
        "language" => NULL,
        "type" => "asset",
        "data" => "",
        "siteId" => NULL,
        "creationDate" => NULL,
        "modificationDate" => 1563348297
    ],
    8 => [
        "id" => 8,
        "name" => "bam.restrictions.group.guest",
        "language" => "",
        "type" => "object",
        "data" => 4,
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1564062248
    ],
    9 => [
        "id" => 9,
        "name" => "bam.restrictions.login.redirect",
        "language" => "de",
        "type" => "document",
        "data" => 2,
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1564062257
    ],
    10 => [
        "id" => 10,
        "name" => "bam.footer.imprint",
        "language" => "de",
        "type" => "document",
        "data" => 6,
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1564064849
    ],
    11 => [
        "id" => 11,
        "name" => "bam.footer.privacy",
        "language" => NULL,
        "type" => "document",
        "data" => "",
        "siteId" => NULL,
        "creationDate" => NULL,
        "modificationDate" => 1563543081
    ],
    12 => [
        "id" => 12,
        "name" => "bam.footer.contact",
        "language" => NULL,
        "type" => "document",
        "data" => "",
        "siteId" => NULL,
        "creationDate" => NULL,
        "modificationDate" => 1563543092
    ],
    13 => [
        "id" => 13,
        "name" => "bam.root.document",
        "language" => "en",
        "type" => "document",
        "data" => NULL,
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1564062224
    ],
    14 => [
        "id" => 14,
        "name" => "bam.restrictions.login.redirect",
        "language" => "en",
        "type" => "document",
        "data" => NULL,
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1564062226
    ],
    15 => [
        "id" => 15,
        "name" => "bam.custom.brand",
        "language" => "",
        "type" => "text",
        "data" => "Stiftung<br>St. Franziskus",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1564407382
    ],
    16 => [
        "id" => 16,
        "name" => "elements.picture.quality",
        "language" => "",
        "type" => "text",
        "data" => "85",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1565353080
    ],
    17 => [
        "id" => 17,
        "name" => "elements.picture.mediaQuery.m",
        "language" => "",
        "type" => "text",
        "data" => "992",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1565352208
    ],
    18 => [
        "id" => 18,
        "name" => "elements.picture.mediaQuery.l",
        "language" => "",
        "type" => "text",
        "data" => "1200",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1565352202
    ],
    19 => [
        "id" => 19,
        "name" => "elements.picture.width",
        "language" => "",
        "type" => "text",
        "data" => "768",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1565342838
    ],
    20 => [
        "id" => 20,
        "name" => "elements.picture.width.3col.m",
        "language" => "",
        "type" => "text",
        "data" => "320",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1565343083
    ],
    21 => [
        "id" => 21,
        "name" => "elements.picture.width.5col.m",
        "language" => "",
        "type" => "text",
        "data" => "128",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1565343136
    ],
    22 => [
        "id" => 22,
        "name" => "elements.picture.width.2col.m",
        "language" => "",
        "type" => "text",
        "data" => "450",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1565343079
    ],
    23 => [
        "id" => 23,
        "name" => "elements.picture.width.1col.m",
        "language" => "",
        "type" => "text",
        "data" => "992",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1565343073
    ],
    24 => [
        "id" => 24,
        "name" => "elements.picture.width.4col.m",
        "language" => "",
        "type" => "text",
        "data" => "256",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1565343162
    ],
    25 => [
        "id" => 25,
        "name" => "elements.picture.width.6col.m",
        "language" => "",
        "type" => "text",
        "data" => "56",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1565343150
    ],
    26 => [
        "id" => 26,
        "name" => "elements.picture.width.1col.l",
        "language" => "",
        "type" => "text",
        "data" => "992",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1565343243
    ],
    27 => [
        "id" => 27,
        "name" => "elements.picture.width.2col.l",
        "language" => "",
        "type" => "text",
        "data" => "450",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1565358410
    ],
    28 => [
        "id" => 28,
        "name" => "elements.picture.width.3col.l",
        "language" => "",
        "type" => "text",
        "data" => "320",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1565343254
    ],
    29 => [
        "id" => 29,
        "name" => "elements.picture.width.4col.l",
        "language" => "",
        "type" => "text",
        "data" => "256",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1565343258
    ],
    30 => [
        "id" => 30,
        "name" => "elements.picture.width.5col.l",
        "language" => "",
        "type" => "text",
        "data" => "128",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1565343262
    ],
    31 => [
        "id" => 31,
        "name" => "elements.picture.width.6col.l",
        "language" => "",
        "type" => "text",
        "data" => "56",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1565343266
    ]
];
