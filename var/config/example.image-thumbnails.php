<?php 

return [
    "portrait-256x256" => [
        "items" => [
            [
                "method" => "scaleByHeight",
                "arguments" => [
                    "height" => 256,
                    "forceResize" => TRUE
                ]
            ],
            [
                "method" => "cover",
                "arguments" => [
                    "width" => 256,
                    "height" => 256,
                    "positioning" => "center",
                    "forceResize" => TRUE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "portrait-256x256",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1562591134,
        "creationDate" => 1562589169,
        "id" => "portrait-256x256"
    ],
    "landscape-256x256" => [
        "items" => [
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 256,
                    "forceResize" => TRUE
                ]
            ],
            [
                "method" => "cover",
                "arguments" => [
                    "width" => 256,
                    "height" => 256,
                    "positioning" => "center",
                    "forceResize" => TRUE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "landscape-256x256",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1562591134,
        "creationDate" => 1562589169,
        "id" => "landscape-256x256"
    ],
    "landscape-256" => [
        "items" => [
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 256,
                    "forceResize" => TRUE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "landscape-256",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1562666818,
        "creationDate" => 1562666801,
        "id" => "landscape-256"
    ],
    "landscape-800" => [
        "items" => [
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 800,
                    "forceResize" => TRUE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "landscape-800",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1562666818,
        "creationDate" => 1562666801,
        "id" => "landscape-800"
    ],
    "portrait-256" => [
        "items" => [
            [
                "method" => "scaleByHeight",
                "arguments" => [
                    "height" => 256,
                    "forceResize" => TRUE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "portrait-256",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1562835951,
        "creationDate" => 1562835926,
        "id" => "portrait-256"
    ],
    "portrait-800" => [
        "items" => [
            [
                "method" => "scaleByHeight",
                "arguments" => [
                    "height" => 800,
                    "forceResize" => TRUE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "portrait-800",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1562835951,
        "creationDate" => 1562835926,
        "id" => "portrait-800"
    ]
];
